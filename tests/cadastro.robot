***Settings***
Resource    ../resources/base.robot
Test Setup      Open Session
Test Teardown   Close Session

***Variables***
${NAME}=       id=io.qaninja.android.twp:id/etUsername
${EMAIL}=      id=io.qaninja.android.twp:id/etEmail  
${PASS}=       id=io.qaninja.android.twp:id/etPassword 
${SPINNER}=    id=io.qaninja.android.twp:id/spinnerJob 

***Test Cases***
Deve realizar o cadastro como QA
    Go To Cadastro Form

    Selecionar o Perfil             QA
    Click Text                      CADASTRAR
    Wait Until Page Contains        Tudo certo, recebemos seus dados!

Deve realizar o cadastro como DevOps
    Go To Cadastro Form

    Selecionar o Perfil             DevOps
    Click Text                      CADASTRAR
    Wait Until Page Contains        Tudo certo, recebemos seus dados!

***Keywords***
Selecionar o Perfil
    [Arguments]     ${alvo}

    Input Text      ${NAME}     Gustavo Carranca
    Input Text      ${EMAIL}    gustavo.carranca@hotmail.com
    Input Text      ${PASS}     naotemsenha
    Click Element   ${SPINNER}
    Wait Until Element Is Visible   class=android.widget.ListView

    Click Text      ${alvo}
