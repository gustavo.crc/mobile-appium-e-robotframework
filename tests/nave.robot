***Settings***
Resource    ../resources/base.robot

# Executa a KW antes de cada testcase
Test Setup      Open Session

#Executa a KW depois de cada testcase
Test Teardown   Close Session

***Variables***
# ${NOME}     Gustavo     # tipo simples
# @{CARROS}   Civic   Corolla   Lancer  Azera   # tipo lista
# &{CARRO}    nome=Corolla    modelo=GT   ano=2014    # tipo objeto

${TOOLBAR_TITLE}    id=io.qaninja.android.twp:id/toolbarTitle


***Test Cases***
Deve abrir a tela dialogs
    Open Menu

    Click Text                      DIALOGS 
    Wait Until Element Is Visible   ${TOOLBAR_TITLE}
    Element Text Should Be          ${TOOLBAR_TITLE}   DIALOGS

Deve abrir a tela formularios
    Open Menu

    Click Text                      FORMS 
    Wait Until Element Is Visible   ${TOOLBAR_TITLE}
    Element Text Should Be          ${TOOLBAR_TITLE}    FORMS

Deve abrir a tela avengers
    Open Menu

    Click Text                      AVENGERS 
    Wait Until Element Is Visible   ${TOOLBAR_TITLE}
    Element Text Should Be          ${TOOLBAR_TITLE}    AVENGERS 