***Settings***
Resource    ../resources/base.robot
Test Setup      Open Session
Test Teardown   Close Session


***Variables***
${ELEMENT}  id=io.qaninja.android.twp:id/short_click

***Test Cases***
Deve realizar um clique simples
    Go To Clique Simples

    Click Element                   ${ELEMENT}
    Wait Until Page Contains        Isso é um clique simples