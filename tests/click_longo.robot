***Settings***
Resource    ../resources/base.robot
Test Setup      Open Session
Test Teardown   Close Session


***Variables***
${ELEMENT}  id=io.qaninja.android.twp:id/long_click

***Test Cases***
Deve realizar um clique longo
    Go To Clique Longo

    Long Press                   ${ELEMENT}     2000
    Wait Until Page Contains     CLIQUE LONGO OK