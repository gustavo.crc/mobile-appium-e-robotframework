***Settings***
Resource    ../resources/base.robot
Test Setup      Open Session
Test Teardown   Close Session

***Variables***
${ELEMENT}  xpath=//android.widget.RadioButton[contains(@text, 'Python')]

***Test Cases***
Deve Selecionar a Opção Python
    Go To Radion Buttons

    Click Element   ${ELEMENT}
    Element Attribute Should Match  ${ELEMENT}   checked    true
