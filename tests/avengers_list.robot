***Settings***
Resource        ../resources/base.robot
Test Setup      Open Session
Test Teardown   Close Session

**Variables***
${REMOVE}=     id=io.qaninja.android.twp:id/btnRemove

***Test Cases***
Deve excluir o Hulk da lista
    Go To Avenger List

    Swipe By Percent    88  62  53  62
    Wait Until Element Is Visible   ${REMOVE}
    Click Element                   ${REMOVE}

#  Y = Horizontal
#  X = Vertical

Deve mover o Iron Men para o topo da lista
    Go To Avenger List

    Drag and Drop   io.qaninja.android.twp:id/drag_handle   2   0