***Settings***
Documentation   Aqui estão todas as KWs helpers

***Variables***
${START}        COMEÇAR
${MENU}         xpath=//android.widget.ImageButton[@content-desc="Open navigation drawer"]
${NAV_VIEW}     id=io.qaninja.android.twp:id/navView

***Keywords***
Get Started
    Wait Until Page Contains        ${START}
    Click Text                      ${START}

Open Menu
    Wait Until Element Is Visible   ${MENU}
    Click Element                   ${MENU}
    Wait Until Element Is Visible   ${NAV_VIEW} 

Go To Login Form
    Open Menu
    Click Text                      FORMS 
    Wait Until Page Contains        FORMS
    
    Click Text                      LOGIN
    Wait Until Page Contains        Fala QA, vamos testar o login?

Go To Cadastro Form
    Open Menu
    Click Text                      FORMS 
    Wait Until Page Contains        FORMS
    
    Click Text                      CADASTRO
    Wait Until Page Contains        Bem-vindo, crie sua conta.

Go To Radion Buttons
    Open Menu
    Click Text                      INPUTS 
    Wait Until Page Contains        INPUTS
    
    Click Text                      BOTÕES DE RADIO
    Wait Until Page Contains        Escolha sua linguagem preferida

Go To Checkbox
    Open Menu
    Click Text                      INPUTS 
    Wait Until Page Contains        INPUTS
    
    Click Text                      CHECKBOX
    Wait Until Page Contains        Marque as techs que usam Appium

Go To Clique Simples
    Open Menu
    Click Text                  BOTÕES
    Wait Until Page Contains    BOTÕES

    Click Text                  CLIQUE SIMPLES
    Wait Until Page Contains    Botão clique simples

Go To Clique Longo
    Open Menu
    Click Text                  BOTÕES
    Wait Until Page Contains    BOTÕES

    Click Text                  CLIQUE LONGO
    Wait Until Page Contains    Botão clique longo

Go To Seek Bar
    Open Menu
    Click Text SEEK Bar         SEEK BAR 
    Wait Until Page Contains    SEEK BAR 

    Click Text                  SEEK 
    Wait Until Page Contains    Seek Bar

Go To Avenger List
    Open Menu
    Click Text                  AVENGERS
    Wait Until Page Contains    AVENGERS

    Click Text                  LISTA
    Wait Until Page Contains    LISTA